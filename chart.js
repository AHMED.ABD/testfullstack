// Cette fonction permet de convertir un string to d 
function formatDate(s) {
    var a = [s.slice(0, 4), "-", s.slice(4, 6), "-", s.slice(6, 8)].join('');
    var newDate = new Date(a)
    return newDate;
}

//selectionner le max des points
function getMaxPoints(playersArr){
	var maxArr = [];
	for(let player in playersArr ){
		maxArr.push(Math.max.apply(null, playersArr[player].points))
	}
	var max = Math.max.apply(null, maxArr);
	max = max + ( 100 - ( max % 100 ));
	return max;  
}

//Obtenir la hauteur de la barre
function getBarHeightPercentage(MaxY,score){
	return (score/MaxY);
}

// Créer une requête
let requete=new XMLHttpRequest();// Créer un objet pour créer des requêtes par la suite
requete.open('GET', 'http://cdn.55labs.com/demo/api.json',false);
//requete.responseType = 'json'; 
requete.send(null);// envoyer la  requête

if (requete.status === 200) {
	var chartjson = JSON.parse(requete.responseText);
} else {
    console.log("Status : %d (%s)", requete.status, requete.statusText);
}

//Définir les couleurs des barres
var colors = {"john":{'color':"#e400ff",'left':'13px'},"larry":{'color':"#ecfc00",'left':'23px'}};
//Obtenir l'histogramme by id
var barChart = document.getElementById('graph');

// Points des joueurs
var players = chartjson.data.DAILY.dataByMember.players;

// Les dates quotidiennes des jeux
var dates = chartjson.data.DAILY.dates;

// settings
var settings = chartjson.settings;

// Obtenir le maximum des points des players
var maxPoint = getMaxPoints(players);

// Le titre de l'histogramme
var title = document.createElement('caption');

title.innerText = settings.label;

barChart.appendChild(title);
//creation de bar row
var barRow = [];

//L'ajout des données au graphique
for (let i = 0; i < dates.length; i++) {
	//créer la ligne des abcisses 
	barRow[i]= document.createElement('tr');
	//the title of our ligne id the date 
	var xAxis = document.createElement('th');
	var dataDate = null;
	var day = null;
	if(dates[i]!== null)
	{
		dataDate = formatDate(dates[i]);
		let month = dataDate.getMonth()+1;
		day = dataDate.getDate()+'/'+month;
	}
	xAxis.innerText = day;
 
	barRow[i].setAttribute('class','qtr');
	if(i == 0)
	{
		barRow[i].style.left = 10 ;
	}
	else {
		barRow[i].style.left = 30  + parseInt(barRow[i - 1].style.left) ;
	}
	var graphHeight = barChart.offsetHeight;
	for (let player in players) {
		let barData = document.createElement('td');
		barData.setAttribute('class','bar');
		barData.style.backgroundColor =  colors[player].color;
		barData.style.left =  colors[player].left;
		barData.style.borderColor = 'transparent';


		barData.style.height = (getBarHeightPercentage(getMaxPoints(players),players[player].points[i]) * graphHeight) + "px";
		//créer details div
		let divDetails = document.createElement('div'); 
		//Définir un id unique pour cette div
		divDetails.setAttribute('id','div' + players[player].points[i] + i + player);
		
		// le contenu de cette div
		let barText = document.createElement('p');
		barText.innerText = 'Score : '  + players[player].points[i];
		
		//définir un id unique pour p players
		barText.setAttribute('id','p'+players[player].points[i] + i + player)
		
		// ajouter la date à div details, changer dataDate par day si vous voulez afficher uniquement le jour
		let barDate = document.createElement('p');
		barDate.innerText = dataDate;
		
		// Initialiser la prorièté display
		divDetails.style.display = "none";
		//rajouter le contenu de barText et barDate   dans divDetails via appendChild
		divDetails.appendChild(barText);
		divDetails.appendChild(barDate);
		/*
		** Définir onClick event listner (l'écouteur d'événements)
		** lorsque l'utilisateur appuye sur la barre, show dtails
		 */
		barData.onclick = function(event) {
			
			// obtenir le detail en utilisant l'id
			let pElement = document.getElementById(event.srcElement.firstChild.id);
			
			/*
			** vérifier si l'élément est visible
            ** si oui make hidden
            ** sinon make it  visible
			*/
			if(pElement.style.display == 'block'){
					pElement.style.display = 'none';
					window.location.href  =  window.location.href.split('#')[0];
			}
			else{
			
					//vérifiez si un autre div est visible, il faudra  le masquer et le supprimer de l'URL
					var idPerviousDiv = window.location.href.split('#')[1]
					console.log(idPerviousDiv)
					if(idPerviousDiv){
						let previousDiv = document.getElementById(idPerviousDiv);
						previousDiv.style.display = 'none';
						pElement.style.display = 'block';
					}
					pElement.style.display = 'block';
					window.location.href = window.location.href.split('#')[0] + "#" + event.srcElement.firstChild.id;			
				}
		};
		window.onload = function(){ 
			let idDiv = window.location.href.split('#')[1];
			if(idDiv){
				let pElement = document.getElementById(idDiv);
				pElement.style.display = 'block';
			}
		}
		
		barData.appendChild(divDetails);
		barRow[i].appendChild(barData);

	}


	barRow[i].appendChild(xAxis);
	barChart.appendChild(barRow[i]);
}

var barchartTemplate = document.getElementById('ticks');
var maxY = getMaxPoints(players);
var step = maxY/5;
for (let i = 0 ; i < 5 ; i++){
	var templateRow = document.createElement('div');
	templateRow.setAttribute('class','tick');
	templateRow.style.height = '60px';
	var yAxis = document.createElement('p');
	yAxis.innerText = maxY;
	templateRow.appendChild(yAxis);
	barchartTemplate.appendChild(templateRow);
	maxY -= step;
	
}

// la légende
var legendEle = document.getElementById('legend');

for(let player in settings.dictionary){
	var _li= document.createElement('li');
	var _span = document.createElement('span');
	_span.setAttribute('id',player);
	_span.setAttribute('class','paid');
	_span.style.backgroundColor = colors[player].color;
	_li.innerText = settings.dictionary[player].firstname + ' ' + settings.dictionary[player].lastname ;
	_li.appendChild(_span);
	legendEle.appendChild(_li);
}

